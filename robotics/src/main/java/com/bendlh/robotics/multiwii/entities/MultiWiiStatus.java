package com.bendlh.robotics.multiwii.entities;

import com.bendlh.shared.utils.ByteUtils;
import com.bendlh.shared.utils.Logger;

/**
 * AndroidDrone
 * MultiWiiStatus
 *
 * Created on 15/04/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 */
public class MultiWiiStatus {

    private final long _cycleTimeMicros;
    private final boolean _hasBarometer;
    private final boolean _hasMagnetometer;
    private final boolean _hasGPS;
    private final boolean _hasSonar;
    private final short _systemLoadPercent;

    /*
    sbufWriteU16(dst, 0); // task delta
    sbufWriteU16(dst, 0); // sensors
        sbufWriteU32(dst, 0); // flight modes
        sbufWriteU8(dst, 0); // profile
        sbufWriteU16(dst, constrain(averageSystemLoadPercent, 0, 100));
        sbufWriteU16(dst, 0); // gyro cycle time
        */

    public MultiWiiStatus(byte[] dataPacket) {

        _cycleTimeMicros = ByteUtils.parseUInt16(dataPacket[0], dataPacket[1]);
        // dataPacket[2 - 3] - i2c_errors_count
        _hasBarometer = (dataPacket[4] & 0b01) == 1;
        _hasMagnetometer = ((dataPacket[4] & 0b10) >> 1) == 1;
        _hasGPS = (dataPacket[5] & 0b01) == 1;
        _hasSonar = ((dataPacket[5] & 0b10) >> 1) == 1;
        // Flight mode
        // Profile
        _systemLoadPercent = ByteUtils.parseInt16(ByteUtils.subArray(dataPacket, 11, 13));

        Logger.debug(this, "CycleTimeMicros = " + _cycleTimeMicros
                + ", Has Barometer = " + _hasBarometer
                + ", Has Magnetometer = " + _hasMagnetometer
                + ", Has GPS = " + _hasGPS
                + ", Has Sonar = " + _hasSonar
                + ", System Load Percent = " + _systemLoadPercent + "%");
    }

    public long getCycleTimeMicros() {
        return _cycleTimeMicros;
    }

    public boolean hasBarometer() {
        return _hasBarometer;
    }

    public boolean hasMagnetometer() {
        return _hasMagnetometer;
    }

    public boolean hasGPS() {
        return _hasGPS;
    }

    public boolean hasSonar() {
        return _hasSonar;
    }

    public short getSystemLoadPercent() {
        return _systemLoadPercent;
    }
}
