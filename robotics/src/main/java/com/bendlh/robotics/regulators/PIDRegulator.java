package com.bendlh.robotics.regulators;

/**
 * Drone
 * PIDRegulator - See https://en.wikipedia.org/wiki/PID_controller
 *
 * Created on 09/04/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 */
public class PIDRegulator {

    public static final int DEFAULT_PROPORTIONAL_GAIN = 5; // Contributes to stability, medium-rate responsiveness.
    public static final int DEFAULT_INTEGRAL_GAIN = 4; // Tracking and disturbance rejection, slow-rate responsiveness. May cause oscillations.
    public static final int DEFAULT_DERIVATIVE_GAIN = 2; // Fast-rate responsiveness, sensitive to noise.

    private final int _proportionalGain; // Kp
    private final int _integralGain; // Ki
    private final int _derivativeGain; // Kd
    private final long _frameRate; // dt - Delta time, the time since last update

    private long _integral = 0;
    private int _previousError = 0;

    public PIDRegulator() {
        _proportionalGain = DEFAULT_PROPORTIONAL_GAIN;
        _integralGain = DEFAULT_INTEGRAL_GAIN;
        _derivativeGain = DEFAULT_DERIVATIVE_GAIN;
        _frameRate = 1;
    }

    /**
     * Constructor overriding the default gain values.
     * @param proportionalGain Contributes to stability, medium-rate responsiveness.
     * @param integralGain Tracking and disturbance rejection, slow-rate responsiveness. May cause oscillations.
     * @param derivativeGain Fast-rate responsiveness, sensitive to noise.
     * @param frameRate The update frequency in millis.
     */
    public PIDRegulator(int proportionalGain, int integralGain, int derivativeGain, long frameRate) {
        _proportionalGain = proportionalGain;
        _integralGain = integralGain;
        _derivativeGain = derivativeGain;
        _frameRate = frameRate;
    }

    /**
     * Provides an output to regulate the current state to the reference.
     * @param reference The target value.
     * @param state The current value/state of the system.
     * @return The output to bring the state to the reference.
     */
    public long regulate(int reference, int state) {

        // error = e(t) - Error as a function of time
        int error = reference - state;

        // integral = integral(0 - t) * e(t) * dt
        _integral = _integral + error * _frameRate;

        // derivative = de(t) / dt
        long derivative = (error - _previousError) / _frameRate;

        // output = Kp * error + Ki * integral + Kd * derivative
        long output = _proportionalGain * error + _integralGain * _integral + _derivativeGain * derivative;

        // Update the previous error
        _previousError = error;

        // All done
        return output;
    }
}
