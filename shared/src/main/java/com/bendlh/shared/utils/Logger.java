package com.bendlh.shared.utils;

import android.util.Log;

import com.bendlh.shared.BuildConfig;

/**
 * AndroidDrone
 * Logger
 *
 * Created on 15/04/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 */
public class Logger {

    public static final int LEVEL_DEBUG = 0;
    public static final int LEVEL_INFO = 1;
    public static final int LEVEL_WARN = 2;
    public static final int LEVEL_ERROR = 3;

    public interface Listener {
        void onLogEntry(int level, Object logger, String message);
    }

    private static Listener _listener;

    public static void addListener(Listener listener) {
        _listener = listener;
    }

    public static void debug(Object object, String message) {
        //if(BuildConfig.DEBUG) {
            Log.d(object.getClass().getSimpleName(), message);
            if (_listener != null) _listener.onLogEntry(LEVEL_DEBUG, object, message);
        //}
    }

    public static void info(Object object, String message) {
        //if (BuildConfig.DEBUG) {
            Log.i(object.getClass().getSimpleName(), message);
            if (_listener != null) _listener.onLogEntry(LEVEL_INFO, object, message);
        //}
    }

    public static void warn(Object object, String message) {
        //if(BuildConfig.DEBUG) {
            Log.w(object.getClass().getSimpleName(), message);
            if (_listener != null) _listener.onLogEntry(LEVEL_WARN, object, message);
        //}
    }

    public static void error(Object object, String message) {
        Log.e(object.getClass().getSimpleName(), message);
        if (_listener != null) _listener.onLogEntry(LEVEL_ERROR, object, message);
    }

    public static void error(Object object, Throwable error) {
        Log.e(object.getClass().getSimpleName(), error.toString());
        Log.e(object.getClass().getSimpleName(), "Printing stack trace:");
        error.printStackTrace();
        if (_listener != null) _listener.onLogEntry(LEVEL_ERROR, object, error.toString());
    }
}
