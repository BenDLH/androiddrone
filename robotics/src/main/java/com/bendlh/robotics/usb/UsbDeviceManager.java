package com.bendlh.robotics.usb;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.hardware.usb.UsbManager;
import android.support.annotation.Nullable;
import android.util.Pair;

import java.util.ArrayList;
import java.util.List;

/**
 * AndroidDrone
 * UsbDeviceManager
 *
 * Created on 09/04/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 */
public class UsbDeviceManager extends BroadcastReceiver {

    private static Context _context;
    private static UsbDeviceManager _instance;

    private final UsbManager _usbManager;

    public static void initialise(Context context) {
        _context = context.getApplicationContext();
        _instance = new UsbDeviceManager();
    }

    public static UsbDeviceManager get() {
        return _instance;
    }

    private UsbDeviceManager() {
        _context.registerReceiver(this, new IntentFilter(UsbManager.ACTION_USB_DEVICE_DETACHED));
        _usbManager = _context.getSystemService(UsbManager.class);
    }

    // region Public API

    public List<UsbDevice> getConnectedDevices() {
        return new ArrayList<>(_usbManager.getDeviceList().values());
    }

    @Nullable
    public UsbDevice getDevice(String productName) {
        for (UsbDevice usbDevice : getConnectedDevices()) {
            if (usbDevice.getProductName() != null && usbDevice.getProductName().contentEquals(productName)) {
                return usbDevice;
            }
        }

        return null;
    }

    @Nullable
    public Pair<UsbDevice, UsbDeviceConnection> connectToDevice(String productName) {
        for (UsbDevice usbDevice : getConnectedDevices()) {
            if (usbDevice.getProductName() != null && usbDevice.getProductName().contentEquals(productName)) {
                return new Pair<>(usbDevice, _usbManager.openDevice(usbDevice));
            }
        }
        return null;
    }

    // endregion

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent.getAction().equals(UsbManager.ACTION_USB_DEVICE_DETACHED)) {

            UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
            if (device != null) {
                //printStatus(getString(R.string.status_removed));
                //printDeviceDescription(device);
            }
        }
    }
}
