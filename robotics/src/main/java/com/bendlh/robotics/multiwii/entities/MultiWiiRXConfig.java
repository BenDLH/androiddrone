package com.bendlh.robotics.multiwii.entities;

import com.bendlh.shared.utils.ByteUtils;
import com.bendlh.shared.utils.Logger;

import java.util.Arrays;

/**
 * AndroidDrone
 * MultiWiiRxConfig
 *
 * Created on 20/04/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 */
public class MultiWiiRXConfig {

    private int[] data;
    /*
    sbufWriteU8(dst, rxConfig()->serialrx_provider);
    sbufWriteU16(dst, rxConfig()->maxcheck);
    sbufWriteU16(dst, rxConfig()->midrc);
    sbufWriteU16(dst, rxConfig()->mincheck);
    sbufWriteU8(dst, rxConfig()->spektrum_sat_bind);
    sbufWriteU16(dst, rxConfig()->rx_min_usec);
    sbufWriteU16(dst, rxConfig()->rx_max_usec);
    sbufWriteU8(dst, rxConfig()->rcInterpolation);
    sbufWriteU8(dst, rxConfig()->rcInterpolationInterval);
    sbufWriteU16(dst, rxConfig()->airModeActivateThreshold);
    sbufWriteU8(dst, rxConfig()->rx_spi_protocol);
    sbufWriteU32(dst, rxConfig()->rx_spi_id);
    sbufWriteU8(dst, rxConfig()->rx_spi_rf_channel_count);
    sbufWriteU8(dst, rxConfig()->fpvCamAngleDegrees);
    */
    public MultiWiiRXConfig(byte[] dataPacket) {
        data = new int[dataPacket.length / 2];
        for (int index = 0; index < data.length; index++) {
            data[index] = ByteUtils.parseUInt8(dataPacket[index]);
        }

        // [0, 108, 7, -36, 5, -80, 4, 0, 117, 3, 67, 8, 2, 19, 70, 5, 0, 0, 0, 0, 0, 0, 0, 82]
        Logger.debug(this, "MultiWiiRXConfig = " + Arrays.toString(dataPacket));
    }
}
