package com.bendlh.robotics.multiwii;

import com.bendlh.shared.utils.ByteUtils;

/**
 * AndroidDrone
 * MSPSpec - The MultiWii Serial Protocol specification
 *
 * Created on 15/04/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 */
public class MSPSpec {

    // Getter commands and data sizes

    public static final int MSP_API_VERSION = 1;
    public static final int MSP_API_VERSION_DATA_SIZE = 3;

    // E/MultiWii: Reply isn't expected length (42 bytes) for command Unknown Command (44)(read 29 bytes)
    // E/MultiWiiRXConfig: MultiWiiRXConfig = [0, 108, 7, -36, 5, -80, 4, 0, 117, 3, 67, 8, 2, 19, 70, 5, 0, 0, 0, 0, 0, 0, 0, 82, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    public static final int MSP_RX_CONFIG = 44;
    public static final int MSP_RX_CONFIG_DATA_SIZE = 23;

    public static final int MSP_RX_MAP = 64;
    public static final int MSP_RX_MAP_DATA_SIZE = 8;
    public static final int MSP_SET_RX_MAP = 65;

    public static final int MSP_FAILSAFE_CONFIG = 75; // TODO
    public static final int MSP_FAILSAFE_CONFIG_DATA_SIZE = 50;

    public static final int MSP_SET_FAILSAFE_CONFIG = 76; // TODO

    public static final int MSP_STATUS = 101;
    public static final int MSP_STATUS_DATA_SIZE = 15;

    public static final int MSP_RAW_IMU = 102;
    public static final int MSP_RAW_IMU_DATA_SIZE = 20;

    public static final int MSP_SERVO_VALUE = 103;

    public static final int MSP_MOTOR_VALUE = 104;
    public static final int MSP_SET_MOTOR_VALUE = 214;

    public static final int MSP_RC_VALUES = 105;
    public static final int MSP_RC_VALUES_DATA_SIZE = 36; // 64 according to docs, 24 from testing with Flip32
    public static final int MSP_SET_RC_VALUES = 200;

    public static final int MSP_RAW_GPS = 106;
    public static final int MSP_SET_RAW_GPS = 201;

    public static final int MSP_COMP_GPS = 107;

    public static final int MSP_ATTITUDE = 108;
    public static final int MSP_ATTITUDE_DATA_SIZE = 6;

    public static final int MSP_ALTITUDE = 109;

    public static final int BIND = 240;

    // MultiWii aircraft types

    public static final int TYPE_TRI = 1;
    public static final int TYPE_QUAD_P = 2;
    public static final int TYPE_QUAD_X = 3;
    public static final int TYPE_BI = 4;
    //....
    public static final int TYPE_AIRPLANE = 14;

    // Data constants

    public static final String PREAMBLE_REQUEST = "$M<";
    public static final String PREAMBLE_REPLY = "$M>";
    public static final String PREAMBLE_ERROR = "$M!";

    public static String getTypeAsString(int type) {
        switch (type) {
            case TYPE_TRI: return "Tri";
            case TYPE_QUAD_P: return "Quad P";
            case TYPE_QUAD_X: return "Quad X";
            case TYPE_BI: return "Bi";
            // ...
            case TYPE_AIRPLANE: return "Airplane";
            default: return "Unknown Type (" + type + ")";
        }
    }

    public static String getCommandAsString(int commandCode) {
        switch (commandCode) {
            case MSP_API_VERSION: return "Get Api Version";
            case MSP_RX_CONFIG: return "Get RX Config";
            case MSP_RX_MAP: return "Get RX Map";
            case MSP_STATUS: return "Get Status";
            case MSP_RAW_IMU: return "Get Raw IMU";
            // ...
            case MSP_RC_VALUES: return "Get RC Values";
            case MSP_ATTITUDE: return "Get Attitude";
            case MSP_ALTITUDE: return "Get Altitude";
            // ...
            case MSP_SET_RC_VALUES: return "Set RC Values";
            case MSP_SET_RX_MAP: return "Set RX Map";
            default: return "Unknown Command (" + commandCode + ")";
        }
    }

    /**
     * This method creates the message that will be sent to the MultiWii
     *
     * Message format is as follows:
     * +--------+---------+----+-------+----+---+
     * |preamble|direction|size|command|data|crc|
     * +--------+---------+----+-------+----+---+
     *
     * Preamble (2 bytes):
     *      Marks the start of a new message; always "$M"
     *
     * Direction (1 byte):
     *      '<' for a command going to the MultiWii. '>' will be present in
     *      information coming from the MultiWii
     *
     * Size (1 byte):
     *      The number of bytes in the payload
     *
     * Command (1 byte):
     *      The message ID of the command, as defined in the protocol
     *      100's for requesting data, and 200's for requesting an action
     *
     * Data (variable bytes):
     *      The data to pass along with the command
     *
     * CRC (1 byte):
     *      Calculated with an XOR of the size, command, and each byte of data
     */
    public static byte[] createMessage(int commandCode, byte[] dataPacket) {

        byte[] preambleBytes = PREAMBLE_REQUEST.getBytes();

        byte[] payload = new byte[preambleBytes.length // Preamble
                                    + 2 // Size and command code
                                    + dataPacket.length // Data packet
                                    + 1]; // Checksum

        // Transfer the preamble bytes
        for (int index = 0; index < preambleBytes.length; index++) {
            payload[index] = preambleBytes[index];
        }

        // Transfer the dataPacket size
        payload[preambleBytes.length] = (byte) dataPacket.length;

        // Transfer command code
        payload[preambleBytes.length + 1] = (byte) commandCode;

        // Transfer the dataPacket
        for (int index = 0; index < dataPacket.length; index++) {
            payload[preambleBytes.length + 2 + index] = dataPacket[index];
        }

        // Transfer the checksum
        payload[preambleBytes.length + 2 + dataPacket.length] = ByteUtils.generateCheckSum(ByteUtils.subArray(payload, 3));

        return payload;
    }

    public static byte[] createMessage(int commandCode) {
        return createMessage(commandCode, new byte[0]);
    }
}
