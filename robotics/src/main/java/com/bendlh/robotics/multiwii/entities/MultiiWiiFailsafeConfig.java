package com.bendlh.robotics.multiwii.entities;

import com.bendlh.shared.utils.ByteUtils;

/**
 * AndroidDrone
 * MultiiWiiFailsafeConfig
 *
 * Created on 20/04/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 */
public class MultiiWiiFailsafeConfig {

    private final int _failsafeDelay;
    private final int _failsafeOffDelay;
    private final short _failsafeThrottle;
    private final int _failsafeKillSwitch;
    private final short _failsafeThrottleLowDelay;
    private final int _failsafeProcedure;

    public MultiiWiiFailsafeConfig(byte[] dataPacket) {

        _failsafeDelay = ByteUtils.parseUInt8(dataPacket[0]);
        _failsafeOffDelay = ByteUtils.parseUInt8(dataPacket[1]);
        _failsafeThrottle = ByteUtils.parseInt16(ByteUtils.subArray(dataPacket, 2, 4));
        _failsafeKillSwitch = ByteUtils.parseUInt8(dataPacket[4]);
        _failsafeThrottleLowDelay = ByteUtils.parseInt16(ByteUtils.subArray(dataPacket, 5, 7));
        _failsafeProcedure = ByteUtils.parseUInt8(dataPacket[0]);
    }
}
