package com.bendlh.robotics.multiwii;

import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;

import com.bendlh.robotics.multiwii.entities.MultiWiiAltitude;
import com.bendlh.robotics.multiwii.entities.MultiWiiApiVersion;
import com.bendlh.robotics.multiwii.entities.MultiWiiAttitude;
import com.bendlh.robotics.multiwii.entities.MultiWiiRCValues;
import com.bendlh.robotics.multiwii.entities.MultiWiiRXConfig;
import com.bendlh.robotics.multiwii.entities.MultiWiiRXMap;
import com.bendlh.robotics.multiwii.entities.MultiWiiRawIMU;
import com.bendlh.robotics.multiwii.entities.MultiWiiResponse;
import com.bendlh.robotics.multiwii.entities.MultiWiiStatus;
import com.bendlh.robotics.usb.serial.Cp21xxSerialDriver;
import com.bendlh.robotics.usb.serial.UsbSerialPort;
import com.bendlh.shared.utils.ByteUtils;
import com.bendlh.shared.utils.Logger;

import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * AndroidDrone
 * MultiWii
 *
 * Created on 15/04/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 */
public class MultiWii {

    private static final int TIMEOUT_MILLIS = 1000;

    private final UsbSerialPort _serialPort;

    public static MultiWii createForDevice(UsbDevice device, UsbDeviceConnection deviceConnection) {
        return new MultiWii(device, deviceConnection);
    }

    private MultiWii(UsbDevice device, UsbDeviceConnection deviceConnection) {

        Cp21xxSerialDriver serialDriver = new Cp21xxSerialDriver(device);

        // Read some data! Most have just one port (port 0).
        _serialPort = serialDriver.getPorts().get(0);

        try {
            _serialPort.open(deviceConnection);
            _serialPort.setParameters(115200, 8, UsbSerialPort.STOPBITS_1, UsbSerialPort.PARITY_NONE);
        } catch (IOException e) {
            Logger.error(this, "Failed to open serial port");
            Logger.error(this, e);
        }
    }

    // region Public API

    public Observable<MultiWiiApiVersion> getApiVersion() {
        return runAsync(() -> {

            // Send off the request
            writeToSerial(MSPSpec.createMessage(MSPSpec.MSP_API_VERSION));

            // Return the result
            return new MultiWiiApiVersion(readFromSerial(MSPSpec.MSP_API_VERSION, MSPSpec.MSP_API_VERSION_DATA_SIZE).getDataPacket());
        });
    }

    public Observable<MultiWiiRXConfig> getRXConfig() {
        return runAsync(() -> {

            // Send off the request
            writeToSerial(MSPSpec.createMessage(MSPSpec.MSP_RX_CONFIG));

            // Return the result
            return new MultiWiiRXConfig(readFromSerial(MSPSpec.MSP_RX_CONFIG, MSPSpec.MSP_RX_CONFIG_DATA_SIZE).getDataPacket());
        });
    }

    public Observable<MultiWiiRXMap> getRXMap() {
        return runAsync(() -> {

            // Send off the request
            writeToSerial(MSPSpec.createMessage(MSPSpec.MSP_RX_MAP));

            // Return the result
            return new MultiWiiRXMap(readFromSerial(MSPSpec.MSP_RX_MAP, MSPSpec.MSP_RX_MAP_DATA_SIZE).getDataPacket());
        });
    }

    public Observable<MultiWiiRXMap> setRXMap(MultiWiiRXMap multiWiiRXMap) {
        return runAsync(() -> {

            // Send off the request
            writeToSerial(MSPSpec.createMessage(MSPSpec.MSP_SET_RX_MAP, multiWiiRXMap.toDataPacket()));

            // Read the result to clear it
            readFromSerial(MSPSpec.MSP_SET_RX_MAP, MSPSpec.MSP_RX_MAP_DATA_SIZE);

            // Return the result
            return new MultiWiiRXMap(readFromSerial(MSPSpec.MSP_RX_MAP, MSPSpec.MSP_RX_MAP_DATA_SIZE).getDataPacket());
        });
    }

    public Observable<MultiWiiStatus> getStatus() {
        return runAsync(() -> {

            // Send off the request
            writeToSerial(MSPSpec.createMessage(MSPSpec.MSP_STATUS));

            // Return the result
            return new MultiWiiStatus(readFromSerial(MSPSpec.MSP_STATUS, MSPSpec.MSP_STATUS_DATA_SIZE).getDataPacket());
        });
    }

    public Observable<MultiWiiRawIMU> getRawIMU() {
        return runAsync(() -> {

            // Send off the request
            writeToSerial(MSPSpec.createMessage(MSPSpec.MSP_RAW_IMU));

            // Return the result
            return new MultiWiiRawIMU(readFromSerial(MSPSpec.MSP_RAW_IMU, MSPSpec.MSP_RAW_IMU_DATA_SIZE).getDataPacket());
        });
    }

    public Observable<MultiWiiAttitude> getAttitude() {
        return runAsync(() -> {

            // Send off the request
            writeToSerial(MSPSpec.createMessage(MSPSpec.MSP_ATTITUDE));

            // Return the result
            return new MultiWiiAttitude(readFromSerial(MSPSpec.MSP_ATTITUDE, MSPSpec.MSP_ATTITUDE_DATA_SIZE).getDataPacket());
        });
    }

    public Observable<MultiWiiAltitude> getAltitude() {
        return runAsync(() -> {

            // Send off the request
            writeToSerial(MSPSpec.createMessage(MSPSpec.MSP_ALTITUDE));

            // Return the result
            return new MultiWiiAltitude(readFromSerial(MSPSpec.MSP_ALTITUDE, MSPSpec.MSP_ATTITUDE_DATA_SIZE).getDataPacket());
        });
    }

    public Observable<MultiWiiRCValues> getRCValues() {
        return runAsync(() -> {

            // Send off the request
            writeToSerial(MSPSpec.createMessage(MSPSpec.MSP_RC_VALUES));

            byte[] dataPacket = readFromSerial(MSPSpec.MSP_RC_VALUES, MSPSpec.MSP_RC_VALUES_DATA_SIZE).getDataPacket();
            //Logger.debug(this, "getRCValues dataPacket = " + Arrays.toString(dataPacket));
            //Logger.debug(this, "getRCValues message = " + Arrays.toString(dataPacket));
            MultiWiiRCValues readRCValues = new MultiWiiRCValues(dataPacket);

            /*MultiWiiRCValues createdRCValues = new MultiWiiRCValues();
            for (int index = 0; index < MultiWiiRCValues.MAX_CHANNEL_COUNT; index++) {
                createdRCValues.setChannelValue(index, readRCValues.getChannelValue(index));
            }
            Logger.debug(this, "Created dataPacket = " + Arrays.toString(createdRCValues.toDataPacket()));
            Logger.debug(this, "Created message = " + Arrays.toString(MSPSpec.createMessage(MSPSpec.MSP_RC_VALUES, createdRCValues.toDataPacket())));
            */

            // Return the result
            return readRCValues;
        });
    }

    public Observable<MultiWiiRCValues> setRCValues(MultiWiiRCValues values) {
        return runAsync(() -> {

            // Send off the write request
            writeToSerial(MSPSpec.createMessage(MSPSpec.MSP_SET_RC_VALUES, values.toDataPacket()));

            // Sent - [36, 77, 60, 14, -56, -36, 5, -36, 5, -36, 5, 117, 3, 117, 3, 117, 3, 117, 3, 31]
            // Read - [36, 77, 62, 14, 105, -36, 5, -36, 5, -36, 5, 117, 3, 14, 6, -36, 5, -36, 5, -64]

            // Read the result to clear it from the serial line
            readFromSerial(MSPSpec.MSP_SET_RC_VALUES, 0);

            // Send off a read request
            writeToSerial(MSPSpec.createMessage(MSPSpec.MSP_RC_VALUES));

            // Return the result
            return new MultiWiiRCValues(readFromSerial(MSPSpec.MSP_RC_VALUES, MSPSpec.MSP_RC_VALUES_DATA_SIZE).getDataPacket());
        });
    }

    // endregion

    // region Internal Methods

    private <T> Observable<T> runAsync(Callable<T> callable) {
        return Observable.fromCallable(callable)
                .delay(10, TimeUnit.MILLISECONDS)
                .subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private synchronized void writeToSerial(byte[] command) throws IOException {
        _serialPort.purgeHwBuffers(true, true);
        _serialPort.write(command, TIMEOUT_MILLIS);
    }

    private synchronized MultiWiiResponse readFromSerial(int command, int responseSize) throws IOException {
        byte buffer[] = new byte[5 + responseSize + 1]; // 3 bytes preamble, 1 byte data size, 1 byte command code and then 1 byte checksum

        int numBytesRead = _serialPort.read(buffer, TIMEOUT_MILLIS);
        //Logger.debug(this, "Read from serial: " + Arrays.toString(buffer));

        if (numBytesRead != buffer.length) {
            Logger.error(this, "Reply isn't expected length (" + buffer.length + " bytes) for command " + MSPSpec.getCommandAsString(command) + "(read " + numBytesRead + " bytes)");
        }
        if (!new String(buffer).substring(0, 3).contentEquals(MSPSpec.PREAMBLE_REPLY)) {

            if (new String(buffer).substring(0, 3).contentEquals(MSPSpec.PREAMBLE_ERROR)) {
                Logger.error(this, "Reply contains error preamble (" + MSPSpec.PREAMBLE_ERROR + ")");
            } else {
                Logger.error(this, "Reply doesn't contain preamble (" + new String(buffer).substring(0, 3) + ")");
            }
        }
        if (!ByteUtils.matchesCheckSum(ByteUtils.subArray(buffer, 3, buffer.length - 1), buffer[buffer.length - 1])) {
            Logger.error(this, "Checksum doesn't match! Expected: " + ByteUtils.generateCheckSum(ByteUtils.subArray(buffer, 3, buffer.length - 1)) + ", received: " + buffer[buffer.length - 1]);
        }

        // Read the data packet size
        //Logger.debug(this, "Reply size is " + buffer[3] + " bytes");

        // Read the command
        if (command == ByteUtils.parseUInt8(buffer[4])) {
            //Logger.debug(this, "Reply command code: " + buffer[4] + ", expected command code: " + command);
        } else {
            Logger.error(this, "Reply command code: " + MSPSpec.getCommandAsString(ByteUtils.parseUInt8(buffer[4])) + ", expected command code: " + MSPSpec.getCommandAsString(command));
        }

        // Read the data
        byte[] dataPacket = ByteUtils.subArray(buffer, 5);
        //Logger.debug(this, "Reply data: " + Arrays.toString(dataPacket));

        return new MultiWiiResponse(buffer[4], dataPacket, buffer[3]);
    }

    public void close() {
        try {
            _serialPort.close();
        } catch (IOException e) {
            Logger.error(this, "Failed to close serial port");
            Logger.error(this, e);
        }
    }

    // endregion
}
