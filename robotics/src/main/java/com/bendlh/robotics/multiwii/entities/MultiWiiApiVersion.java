package com.bendlh.robotics.multiwii.entities;

import com.bendlh.shared.utils.ByteUtils;
import com.bendlh.shared.utils.Logger;

/**
 * AndroidDrone
 * MultiWiiApiVersion
 *
 * Created on 17/04/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 */
public class MultiWiiApiVersion {

    private final int _protocolVersion;
    private final String _apiVersion;

    public MultiWiiApiVersion(byte[] dataPacket) {

        _protocolVersion = ByteUtils.parseUInt8(dataPacket[0]);
        _apiVersion = ByteUtils.parseUInt8(dataPacket[1]) + "." + ByteUtils.parseUInt8(dataPacket[1]);

        Logger.debug(this, "MSP Version = " + _protocolVersion + ", API Version = " + _apiVersion);
    }

    public int getVersion() {
        return _protocolVersion;
    }

    public String getApiVersion() {
        return _apiVersion;
    }
}
