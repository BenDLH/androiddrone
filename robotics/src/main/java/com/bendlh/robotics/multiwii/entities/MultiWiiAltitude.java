package com.bendlh.robotics.multiwii.entities;

import com.bendlh.shared.utils.ByteUtils;
import com.bendlh.shared.utils.Logger;

/**
 * AndroidDrone
 * MultiWiiAltitude
 *
 * Created on 15/04/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 */
public class MultiWiiAltitude {

    private final int _estimatedAltitude; // In CM
    private final int _variation; // In CM/S

    public MultiWiiAltitude(byte[] dataPacket) {

        _estimatedAltitude = ByteUtils.parseInt32(ByteUtils.subArray(dataPacket, 0, 4));
        _variation = ByteUtils.parseInt16(ByteUtils.subArray(dataPacket, 4, 6));

        Logger.debug(this, "Estimated Altitude = " + _estimatedAltitude + "cm, Variation = " + _variation + "cm/s");
    }

    public int getEstimatedAltitude() {
        return _estimatedAltitude;
    }

    public int getAltitudeVariation() {
        return _variation;
    }
}
