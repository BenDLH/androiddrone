package com.bendlh.android_drone;

import android.app.Application;

import com.bendlh.robotics.usb.UsbDeviceManager;


/**
 * AndroidDrone
 * CustomApplication
 *
 * Created on 09/04/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 */
public class CustomApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        UsbDeviceManager.initialise(this);
    }
}
