package com.bendlh.robotics.multiwii.entities;

import com.bendlh.shared.utils.ByteUtils;
import com.bendlh.shared.utils.Logger;

/**
 * AndroidDrone
 * MultiWiiRawIMU
 *
 * Created on 15/04/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 */
public class MultiWiiRawIMU {

    private final int _accelerometerX;
    private final int _accelerometerY;
    private final int _accelerometerZ;

    private final int _gyroX;
    private final int _gyroY;
    private final int _gyroZ;

    private final int _magnetometerX;
    private final int _magnetometerY;
    private final int _magnetometerZ;

    public MultiWiiRawIMU(byte[] dataPacket) {
        _accelerometerX = ByteUtils.parseUInt16(dataPacket[0], dataPacket[1]);
        _accelerometerY = ByteUtils.parseUInt16(dataPacket[2], dataPacket[3]);
        _accelerometerZ = ByteUtils.parseUInt16(dataPacket[3], dataPacket[5]);

        _gyroX = ByteUtils.parseUInt16(dataPacket[6], dataPacket[7]);
        _gyroY = ByteUtils.parseUInt16(dataPacket[8], dataPacket[9]);
        _gyroZ = ByteUtils.parseUInt16(dataPacket[10], dataPacket[11]);

        _magnetometerX = ByteUtils.parseUInt16(dataPacket[12], dataPacket[13]);
        _magnetometerY = ByteUtils.parseUInt16(dataPacket[14], dataPacket[15]);
        _magnetometerZ = ByteUtils.parseUInt16(dataPacket[16], dataPacket[17]);

        Logger.debug(this, "AccelerometerX = " + _accelerometerX + ", AccelerometerY = " + _accelerometerY + ", AccelerometerZ = " + _accelerometerZ);
        Logger.debug(this, "GyroX = " + _gyroX + ", GyroY = " + _gyroY + ", GyroZ = " + _gyroZ);
        Logger.debug(this, "MagnetometerX = " + _magnetometerX + ", MagnetometerY = " + _magnetometerY + ", MagnetometerZ = " + _magnetometerZ);
    }
}
