package com.bendlh.robotics.multiwii.entities;

import com.bendlh.shared.utils.ByteUtils;
import com.bendlh.shared.utils.Logger;

/**
 * AndroidDrone
 * MultiWiiFCVersion
 *
 * Created on 17/04/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 */
public class MultiWiiFCVersion {

    private final String _fcVersion;

    public MultiWiiFCVersion(byte[] dataPacket) {

        _fcVersion = ByteUtils.parseUInt8(dataPacket[0]) + "." + ByteUtils.parseUInt8(dataPacket[1]) + "." + ByteUtils.parseUInt8(dataPacket[1]);

        Logger.debug(this, "FC Version = " + _fcVersion);
    }

    public String getVersion() {
        return _fcVersion;
    }
}
