package com.bendlh.robotics.regulators;

import org.junit.Test;

/**
 *
 * AndroidDrone
 * PIDRegulatorTest
 *
 * Created on 09/04/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 *
 */
public class PIDRegulatorTest {

    @Test
    public void testRegulate() {

        System.out.println("----> Test start");

        PIDRegulator regulator = new PIDRegulator();
        int reference = 1000;
        int state = 0;

        int testSteps = 100;
        int score = testSteps;

        for(int index = 0; index < testSteps; index++) {
            state += (regulator.regulate(reference, state) / 10);

            // We only update the score if it's the worst possible, or it's just going to keep updating
            if (state == reference && score == testSteps) {
                score = index;

            } else if (state != reference) {
                // Reset the score to the worst value
                score = testSteps;
            }

            System.out.println("Update " + index + ": state = " + state);
        }

        System.out.println("----> Test end");

        System.out.println("==> Score: " + score);
    }
}
