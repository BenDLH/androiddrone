package com.bendlh.robotics.multiwii.entities;

/**
 * AndroidDrone
 * MultiWiiResponse
 *
 * Created on 15/04/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 */
public class MultiWiiResponse {

    private final int _commandCode;
    private final byte[] _dataPacket;
    private final int _dataSize;

    public MultiWiiResponse(int commandCode, byte[] dataPacket, int dataSize) {
        _commandCode = commandCode;
        _dataPacket = dataPacket;
        _dataSize = dataSize;
    }

    public int getCommandCode() {
        return _commandCode;
    }

    public byte[] getDataPacket() {
        return _dataPacket;
    }

    public int getDataSize() {
        return _dataSize;
    }
}
