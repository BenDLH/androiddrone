package com.bendlh.robotics.multiwii.entities;

import com.bendlh.shared.utils.ByteUtils;
import com.bendlh.shared.utils.Logger;

/**
 * AndroidDrone
 * MultiWiiAttitude
 *
 * Created on 15/04/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 */
public class MultiWiiAttitude {

    private final short _angleX;
    private final short _angleY;
    private final short _heading;

    public MultiWiiAttitude(byte[] dataPacket) {

        _angleX = ByteUtils.parseInt16(ByteUtils.subArray(dataPacket, 0, 2));
        _angleY = ByteUtils.parseInt16(ByteUtils.subArray(dataPacket, 2, 4));
        _heading = ByteUtils.parseInt16(ByteUtils.subArray(dataPacket, 4, 6));

        //if (_angleX < -1800 || _angleY > 1800) Logger.error(this, "Angle X is out of bounds!!");
        //if (_angleY < -900 || _angleY > 900) Logger.error(this, "Angle Y is out of bounds!!");
        //if (_heading < -180 || _heading > 180) Logger.error(this, "Heading is out of bounds!!");

        Logger.debug(this, "AngleX = " + _angleX + ", AngleY = " + _angleY + ", Heading = " + _heading);
    }

    public short getAngleX() {
        return _angleX;
    }

    public short getAngleY() {
        return _angleY;
    }

    public short getHeading() {
        return _heading;
    }
}
