package com.bendlh.robotics.multiwii.entities;

import com.bendlh.shared.utils.Logger;

import java.util.Arrays;

/**
 * AndroidDrone
 * MultiWiiRXMap
 *
 * Created on 20/04/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 */
public class MultiWiiRXMap {

    private final char[] _channelMap;

    public MultiWiiRXMap(byte[] dataPacket) {
        _channelMap = new char[dataPacket.length];
        for (int index = 0; index < _channelMap.length; index++) {
            if (dataPacket[index] < 9) {
                _channelMap[index] = (char) (dataPacket[index] + 48); // Numbers 0 - 9 don't directly convert to ASCII
            } else {
                _channelMap[index] = (char) dataPacket[index];
            }
        }

        Logger.debug(this, "Channel Map = " + Arrays.toString(_channelMap));
    }

    public MultiWiiRXMap(char[] channelMap) {
        _channelMap = channelMap;
    }

    public byte[] toDataPacket() {
        byte[] dataPacket = new byte[_channelMap.length];
        for (int index = 0; index < dataPacket.length; index++) {
            if (dataPacket[index] < 9 + 48) {
                dataPacket[index] = (byte) (_channelMap[index] - 48); // Numbers 0 - 9 don't directly convert from ASCII
            } else {
                dataPacket[index] = (byte) _channelMap[index];
            }
        }

        Logger.debug(this, "Channel Map toDataPacket = " + Arrays.toString(dataPacket));

        return dataPacket;
    }
}
