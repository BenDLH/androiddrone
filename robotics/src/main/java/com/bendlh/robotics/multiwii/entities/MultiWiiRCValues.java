package com.bendlh.robotics.multiwii.entities;

import android.util.SparseIntArray;

import com.bendlh.shared.utils.ByteUtils;

/**
 * AndroidDrone
 * MultiWiiRCValues
 *
 * Created on 16/04/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 */
public class MultiWiiRCValues {

    public static final int CHANNEL_ROLL = 0;
    public static final int CHANNEL_PITCH = 1;
    public static final int CHANNEL_YAW = 2; // TODO
    public static final int CHANNEL_THROTTLE = 3;
    public static final int CHANNEL_AUX1 = 4;
    public static final int CHANNEL_AUX2 = 5;
    public static final int CHANNEL_AUX3 = 6;

    public static final int MAX_CHANNEL_COUNT = 7; // 16 according to MultiWii docs, 7 from testing with Flip32

    public static final int MIN_CHANNEL_VALUE = 1000;
    public static final int MAX_CHANNEL_VALUE = 2000;

    private SparseIntArray _rcValues = new SparseIntArray();

    public MultiWiiRCValues() {}

    public MultiWiiRCValues(byte[] dataPacket) {

        for (int index = 0; index < MAX_CHANNEL_COUNT; index++) {
            _rcValues.append(index, ByteUtils.parseUInt16(dataPacket[index * 2], dataPacket[index * 2 + 1]));
        }

        //Logger.debug(this, "RC Values = " + _rcValues.toString());
    }

    public int getChannelValue(int channel) {
        return _rcValues.get(channel);
    }

    public void setChannelValue(int channel, int value) {
        _rcValues.put(channel, value);
    }

    public SparseIntArray getChannelValues() {
        return _rcValues;
    }

    public byte[] toDataPacket() {

        byte[] dataPacket = new byte[MAX_CHANNEL_COUNT * 2];
        for (int index = 0; index < MAX_CHANNEL_COUNT; index++) {

            int channelValue = _rcValues.get(index);

            byte[] bytes = ByteUtils.serialiseInt16((short) channelValue);
            dataPacket[index * 2] = bytes[0];
            dataPacket[index * 2 + 1] = bytes[1];
        }

        return dataPacket;
    }
}
