package com.bendlh.android_drone;

import android.app.Activity;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbDeviceConnection;
import android.os.Bundle;
import android.util.Pair;
import android.view.LayoutInflater;

import com.bendlh.android_drone.databinding.ActivityMainBinding;
import com.bendlh.robotics.multiwii.MultiWii;
import com.bendlh.robotics.multiwii.entities.MultiWiiRCValues;
import com.bendlh.robotics.multiwii.entities.MultiWiiRXMap;
import com.bendlh.robotics.usb.UsbDeviceManager;
import com.bendlh.shared.utils.Logger;

import java.util.Arrays;

import io.reactivex.functions.Consumer;

public class MainActivity extends Activity {

    public static final String FLIP_32_PRODUCT_NAME = "CP2102 USB to UART Bridge Controller";

    private static final int MIN_THROTTLE = 1000;
    private static final int MAX_THROTTLE = 1200;
    private static final int THROTTLE_STEP = 1;

    private MultiWii _multiWii;

    private boolean _risingThrottle;
    private int _throttleValue;

    private ActivityMainBinding _binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        _binding = ActivityMainBinding.inflate(LayoutInflater.from(this));
        _binding.setVariable(com.bendlh.android_drone.BR.activity, this);
        setContentView(_binding.getRoot());

        Pair<UsbDevice, UsbDeviceConnection> deviceAndConnection = UsbDeviceManager.get().connectToDevice(FLIP_32_PRODUCT_NAME);

        if (deviceAndConnection != null) {
            _multiWii = MultiWii.createForDevice(deviceAndConnection.first, deviceAndConnection.second);

            fetchApiVersion();

        } else {
            Logger.debug(this, "UsbDevice is null!");
        }
    }

    private void fetchApiVersion() {

        // 2017-04-16 @ 17:13:35 -- MultiWii API version received - 1.16.0
        // 2017-04-16 @ 17:13:35 -- Flight controller info, identifier: CLFL, version: 1.12.0
        _multiWii.getApiVersion()
                .subscribe(multiWiiIdentity -> {
                    _binding.version.setText("" + multiWiiIdentity.getVersion());
                    // TODO _binding.type.setText(multiWiiIdentity.getType());
                    _binding.apiVersion.setText("" + multiWiiIdentity.getApiVersion());

                    fetchRXConfig();
                });
    }

    private void fetchRXConfig() {

        _multiWii.getRXConfig()
                .subscribe(multiWiiRXConfig -> {

                    fetchRXMap();
                });
    }

    private void fetchRXMap() {

        _multiWii.getRXMap()
                .subscribe(multiWiiRXMap -> {

                    MultiWiiRXMap createdRXMap = new MultiWiiRXMap(new char[]{'0', '1', '2', '3', '4', '5', '6', '7', 'H'});
                    _multiWii.setRXMap(createdRXMap)
                            .subscribe(new Consumer<MultiWiiRXMap>() {
                                @Override
                                public void accept(MultiWiiRXMap multiWiiRXMap) throws Exception {

                                    Logger.debug(this, "After sending, received RX Map = " + Arrays.toString(multiWiiRXMap.toDataPacket()));
                                    fetchStatus();
                                }
                            });

                });
    }

    private void fetchStatus() {

        _multiWii.getStatus()
                .subscribe(multiWiiStatus -> {
                    _binding.cycleTimeMicros.setText("" + multiWiiStatus.getCycleTimeMicros());
                    _binding.hasBarometer.setText("" + multiWiiStatus.hasBarometer());
                    _binding.hasMagnetometer.setText("" + multiWiiStatus.hasMagnetometer());
                    _binding.hasGPS.setText("" + multiWiiStatus.hasGPS());
                    _binding.hasSonar.setText("" + multiWiiStatus.hasSonar());
                    _binding.systemLoadPercent.setText(multiWiiStatus.getSystemLoadPercent() + "%");

                    fetchAltitude();
                });
    }

    private void fetchAltitude() {

        _multiWii.getAltitude()
                .subscribe(multiWiiAltitude -> {
                    _binding.estimatedAltitude.setText(multiWiiAltitude.getEstimatedAltitude() + "cm");
                    _binding.altitudeVariation.setText(multiWiiAltitude.getAltitudeVariation() + "cm/s");

                    fetchAttitude();
                });
    }

    private void fetchAttitude() {

        _multiWii.getAttitude()
                .subscribe(multiWiiAttitude -> {
                    _binding.angleX.setText("" + multiWiiAttitude.getAngleX());
                    _binding.angleY.setText("" + multiWiiAttitude.getAngleY());
                    _binding.heading.setText("" + multiWiiAttitude.getHeading());

                    fetchRCValues();
                });
    }

    private void fetchRawIMU() {

        _multiWii.getRawIMU()
                .subscribe(multiWiiRawIMU -> {

                });
    }

    private void fetchRCValues() {

        _multiWii.getRCValues()
                .subscribe(multiWiiRCValues -> {

                    _binding.roll.setText("" + multiWiiRCValues.getChannelValue(MultiWiiRCValues.CHANNEL_ROLL));
                    _binding.pitch.setText("" + multiWiiRCValues.getChannelValue(MultiWiiRCValues.CHANNEL_PITCH));
                    _binding.yaw.setText("" + multiWiiRCValues.getChannelValue(MultiWiiRCValues.CHANNEL_YAW));
                    _binding.throttle.setText("" + multiWiiRCValues.getChannelValue(MultiWiiRCValues.CHANNEL_THROTTLE));
                    _binding.aux1.setText("" + multiWiiRCValues.getChannelValue(MultiWiiRCValues.CHANNEL_AUX1));
                    _binding.aux2.setText("" + multiWiiRCValues.getChannelValue(MultiWiiRCValues.CHANNEL_AUX2));
                    _binding.aux3.setText("" + multiWiiRCValues.getChannelValue(MultiWiiRCValues.CHANNEL_AUX3));

                    //Logger.debug(this, "Received RC values = " + multiWiiRCValues.getChannelValues());

                    //Logger.debug(this, "Throttle value = " + _throttleValue);

                    // Set new RC Values
                    MultiWiiRCValues createdRCValues = new MultiWiiRCValues();
                    createdRCValues.setChannelValue(MultiWiiRCValues.CHANNEL_ROLL, 1500);
                    createdRCValues.setChannelValue(MultiWiiRCValues.CHANNEL_PITCH, 1500);
                    createdRCValues.setChannelValue(MultiWiiRCValues.CHANNEL_YAW, 1500);
                    createdRCValues.setChannelValue(MultiWiiRCValues.CHANNEL_THROTTLE, _throttleValue);
                    createdRCValues.setChannelValue(MultiWiiRCValues.CHANNEL_THROTTLE, 885);
                    //createdRCValues.setChannelValue(MultiWiiRCValues.CHANNEL_AUX1, 1700); // 1700+ is armed
                    createdRCValues.setChannelValue(MultiWiiRCValues.CHANNEL_AUX2, 885);
                    createdRCValues.setChannelValue(MultiWiiRCValues.CHANNEL_AUX3, 885);

                    Logger.debug(this, "Sending RC values = " + createdRCValues.getChannelValues());

                    // Write the same values back again
                    _multiWii.setRCValues(createdRCValues)
                            .subscribe(receivedRCValues -> {

                                Logger.debug(this, "After Sending, Received RC values = " + receivedRCValues.getChannelValues());

                                // TODO Infinite loop right here
                                fetchRCValues();
                            });

                    // Update the throttle value
                    _throttleValue = _risingThrottle ? _throttleValue + THROTTLE_STEP : _throttleValue - THROTTLE_STEP;

                    if (_throttleValue <= MIN_THROTTLE) {
                        _risingThrottle = !_risingThrottle;
                        _throttleValue = MIN_THROTTLE;
                    }
                    if (_throttleValue >= MAX_THROTTLE) {
                        _risingThrottle = !_risingThrottle;
                        _throttleValue = MAX_THROTTLE;
                    }
                });
    }
}
