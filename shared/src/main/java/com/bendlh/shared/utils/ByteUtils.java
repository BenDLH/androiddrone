package com.bendlh.shared.utils;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * AndroidDrone
 * ByteUtils
 *
 * Created on 15/04/2017
 * Copyright (c) 2017 SHAPE A/S. All rights reserved.
 */
public class ByteUtils {

    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();
    private static final int NEGATIVE_BITMASK = 0x40000000;

    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static char[] bytesToCharArray(byte[] buffer) {
        char[] chars = new char[buffer.length / 2];
        for(int index = 0; index < chars.length; index++) {
            chars[index] = (char) ((buffer[index * 2] << 8) + (buffer[index * 2 + 1] & 0xFF));
        }

        return chars;
    }

    public static byte[] subArray(byte[] buffer, int startIndex) {
        byte[] subArray = new byte[buffer.length - startIndex];
        for (int index = 0; index < subArray.length; index++) {
            subArray[index] = buffer[startIndex + index];
        }
        return subArray;
    }

    public static byte[] subArray(byte[] buffer, int startIndex, int endIndex) {
        int length = endIndex - startIndex;
        byte[] subArray = new byte[length];
        for (int index = 0; index < length; index++) {
            subArray[index] = buffer[startIndex + index];
        }
        return subArray;
    }

    public static byte generateCheckSum(byte[] data) {
        byte checkSum = 0;
        for (int index = 0; index < data.length; index++) {
            checkSum ^= data[index];
        }
        return checkSum;
    }

    public static boolean matchesCheckSum(byte[] data, byte checkSum) {
        return generateCheckSum(data) == checkSum;
    }


    public static int parseUInt8(byte value) {
        return value & 0xff;
    }

    public static short parseInt16(byte[] bytes) {
        return ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN).getShort();
    }

    public static byte[] serialiseInt16(short value) {
        return ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN).putShort(value).array();
    }

    public static int parseUInt16(byte leastSignificantByte, byte mostSignificantByte) {
        return toSignedInt(((mostSignificantByte & 0xff) << 8) | (leastSignificantByte & 0xff));
    }

    public static int parseUInt32(byte leastSignificantByte, byte smallerByte, byte largerByte, byte mostSignificantByte) {
        return toSignedInt(((mostSignificantByte & 0xff) << 24) | ((smallerByte & 0xff) << 16) | ((largerByte & 0xff) << 8) | (leastSignificantByte & 0xff));
    }

    public static int parseInt32(byte[] bytes) {
        return ByteBuffer.wrap(bytes).order(ByteOrder.LITTLE_ENDIAN).getInt();
    }

    private static int toSignedInt(int unsignedInt) {
        return (unsignedInt << 1) >> 1;
    }
}
